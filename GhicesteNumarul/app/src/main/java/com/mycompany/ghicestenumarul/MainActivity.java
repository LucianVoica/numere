package com.mycompany.ghicestenumarul;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.Random;

class Joc {
    /**
     * o data de tipul Random cu care vom genera numarul random
     */
    Random randomGenerator = new Random();
    /**
     * numarul generat este retinut in vectorul random
     */
    private int[] random = new int[4];

    /**
     * constructorul formeaza numarul random
     */
    public Joc()  {

        random[0]=randomGenerator.nextInt(9)+1;
        for(int i = 1; i < 4; i++) {
            random[i] = randomGenerator.nextInt(10);
        }
    }

    /**
     * verifica cate cifre din numarul dat sunt in numarul random
     * @param a prima cifra
     * @param b a 2-a cifra
     * @param c a 3-a cifta
     * @param d a 4-a cifra
     * @return numarul de cifre
     */
    public int nimerit( int a, int b, int c, int d) {
        int[] numar=new int[4];
        int[] vizitat=new int[4];
        int cateNimerite=0;
        numar[0] = a;
        numar[1] = b;
        numar[2] = c;
        numar[3] = d;
        for(int i = 0; i < 4;i++)
            vizitat[i]=0;
        for(int i = 0; i < 4;i++)
            for(int j = 0; j < 4 && vizitat[i]==0 ; j++)
                if( random[i] == numar[j] ) {
                    vizitat[i] = 1;
                    j = 5;
                    cateNimerite++;
                }
        return cateNimerite;
    }

    /**
     * verifica cate cifre din numarul dat sunt pe aceeasi pozitie ca cifrele numarului random
     * @param a prima cifra
     * @param b a 2-a cifra
     * @param c a 3-a cifra
     * @param d a 4-a cifra
     * @return numarul de cifre aflate pe aceeasi pozitie ca si in numarul random
     */
    public int pePozitie(int a, int b, int c, int d) {
        int nrPePozitie = 0;
        if(a == random[0]) nrPePozitie++;
        if(b == random[1]) nrPePozitie++;
        if(c == random[2]) nrPePozitie++;
        if(d == random[3]) nrPePozitie++;
        return nrPePozitie;
    }

    /**
     * formeaza numarul random din cifrele sale
     * @return numarul random
     */
    public int getNumber(){
        int numar=0;
        for(int i = 0 ; i < 4 ; i++) {
            numar=numar*10+random[i];
        }
        return numar;
    }
}

public class MainActivity extends ActionBarActivity {
    /**
     * obiect de tip Joc
     */
    Joc x = new Joc();
    /**
     * contor pentru a restrange jocul la a avea maxim 25 de incercari
     */
    int contor = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * pentru fiecare apasare de buton verifica numarul dat daca este cel generat
     * @param v view-ul
     */
    public void onButtonClick(View v) {
        //numarul dat de utilizator
        int n=0;
        //tester pentru a vedea daca numarul are 4 cifre
        int numarcorect=1;
        //prima cifra a numarului
        int a;
        //a doua cifra a numarului
        int b;
        //a treia cifra a numarului
        int c;
        //a patra cifra a numarului
        int d;
        //mesajul care va fi afisat de robotel
        String mesaj="";
        //campul unde se afiseaza mesajul
        TextView r1=(TextView)findViewById(R.id.textView);
        //verifica daca nu am depasit numarul de 25 de incercari
        if(contor < 25) {
            //preia numarul dat de utilizator

            try {
                EditText e1 = (EditText) findViewById(R.id.numar);
                n = Integer.parseInt(e1.getText().toString());
            } catch (Exception e) {
                n=0;
                e.printStackTrace();
            }
            mesaj = n + ": ";
            //verific daca numarul are 4 cifre
            if (n < 1000 || n > 9999) {
                mesaj = "Numarul nu are 4 cifre!";
                contor--;
                numarcorect = 0;
            }
            //daca are 4 cifre atunci incepe validarea numarului
            if (numarcorect == 1) {
                d = n % 10;
                c = n / 10 % 10;
                b = n / 100 % 10;
                a = n / 1000;
                if (x.pePozitie(a, b, c, d) == 4) {
                    mesaj = mesaj + "FELICITARI! M-am gandit la numarul " + x.getNumber() + ". L-ai ghicit din " + contor + " incercari.";
                    contor = 26;
                } else {
                    int nimerite;
                    nimerite = x.nimerit(a, b, c, d);
                    if (nimerite == 0)
                        mesaj = mesaj + "Nu ai nimerit nicio cifra :( ";
                    else if (nimerite == 1)
                        mesaj = mesaj + "Ai nimerit o cifra ";
                    else
                        mesaj = mesaj + "Ai nimerit " + nimerite + " cifre ";
                    if (nimerite > 0) {
                        int nrPozitii;
                        nrPozitii = x.pePozitie(a, b, c, d);
                        if (nrPozitii == 0)
                            mesaj = mesaj + " dintre care niciuna nu este pozitionata corect. ";
                        else if (nrPozitii == 1)
                            mesaj = mesaj + " dintre care una este pozitionata corect. ";
                        else
                            mesaj = mesaj + " dintre care " + nrPozitii + " sunt pozitionate corect. ";
                    }
                }
                if (contor == 23)
                    mesaj = mesaj + "ULTIMA INCERCARE!";
                if (contor == 24)
                    mesaj = "Ah, nu ai ghicit. Numarul era " + x.getNumber() + ".";
                contor++;
            }
            } else if(contor>=25) {
                mesaj = "Te mai joci o data?";
                contor = 0;
                x = new Joc();
            }
            //afisez mesajul
            r1.setText(mesaj);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
