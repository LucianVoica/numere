package com.company;
import java.util.Random;
import java.util.Scanner;

class Joc {
    Random randomGenerator = new Random();
    private int[] random = new int[4];

    public Joc()  {

        random[0]=randomGenerator.nextInt(9)+1;
        for(int i = 1; i < 4; i++) {
            random[i] = randomGenerator.nextInt(10);
        }
    }

    public int nimerit( int a, int b, int c, int d) {
        int[] numar=new int[4];
        int[] vizitat=new int[4];
        int cateNimerite=0;
        numar[0] = a;
        numar[1] = b;
        numar[2] = c;
        numar[3] = d;
        for(int i = 0; i < 4;i++)
            vizitat[i]=0;
        for(int i = 0; i < 4;i++)
            for(int j = 0; j < 4 && vizitat[i]==0 ; j++)
                if( random[i] == numar[j] ) {
                    vizitat[i] = 1;
                    j = 5;
                    cateNimerite++;
                }
        return cateNimerite;
    }

    public int pePozitie(int a, int b, int c, int d) {
        int nrPePozitie = 0;
        if(a == random[0]) nrPePozitie++;
        if(b == random[1]) nrPePozitie++;
        if(c == random[2]) nrPePozitie++;
        if(d == random[3]) nrPePozitie++;
        return nrPePozitie;
    }

    public int getNumber(){
        int numar=0;
        for(int i = 0 ; i < 4 ; i++) {
            numar=numar*10+random[i];
        }
        return numar;
    }
}

public class Main {
    public static void main(String argv[]) {
        int n;
        int contor = 0;
        Joc x = new Joc();
        int a, b, c, d;
        Scanner in = new Scanner(System.in);
        System.out.println("Ce bine ca o sa te joci cu mine! Eu m-am gandit deja la un numar. Incearca sa-l ghicesti!");
        System.out.println("Dati numarul dumneavoastra de 4 cifre:");
        while (contor < 25) {
            n = in.nextInt();
            if (n < 1000 || n > 9999) {
                do {
                    System.out.println("Numarul nu are 4 cifre! Incearca din nou.");
                    n = in.nextInt();
                } while (n < 1000 || n > 9999);
            }

            d = n % 10;
            c = n / 10 % 10;
            b = n / 100 % 10;
            a = n / 1000;
            if(x.pePozitie(a, b, c, d) == 4) {
                System.out.println("FELICITARI! M-am gandit la numarul " + n);
                System.out.println("L-ai nimerit din " + contor + " incercari.");
                contor=26;
            }
            else {
                int nimerite;
                nimerite = x.nimerit(a, b, c, d);
                if (nimerite == 0)
                    System.out.println("Nu ai nimerit nicio cifra :( ");
                else if (nimerite == 1)
                    System.out.print("Ai nimerit o cifra ");
                        else
                            System.out.print("Ai nimerit " + nimerite + " cifre ");
                if (nimerite > 0) {
                    int nrPozitii;
                    nrPozitii = x.pePozitie(a, b, c, d);
                    if(nrPozitii == 0)
                        System.out.println(" dintre care niciuna nu este pozitionata corect ");
                    else if (nrPozitii == 1)
                        System.out.println(" dintre care una este pozitionata corect ");
                            else
                                System.out.println(" dintre care " + nrPozitii + " sunt pozitionate corect ");
                }
            }
            if(contor == 23)
                System.out.println("Ai grija! Este ultima incercare!");
            if(contor == 24)
                System.out.println("Ah, nu ai ghicit. Numarul era " + x.getNumber() + ". Joaca din nou!");
            contor++;
        }
    }
}
